<?php
/**
 * Created by PhpStorm.
 * User: alexisbarniquez
 * Date: 9/14/17
 * Time: 17:32
 */

$path = "http://imaginamos.com/developer-test/users.json";
$path1 = "http://imaginamos.com/developer-test/companies.json";

$data = file_get_contents($path);
$data1 = file_get_contents($path1);

$json = json_decode($data, true);
$json1 = json_decode($data1, true);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Imaginamos1</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<div class="container">
	<div class="row busqueda">
		<div class="col-6">
			<div class="input-group">
				<input type="text" class="form-control" id="busc" name="busc" value="" placeholder="..." onkeyup="buscar()">
				<span class="input-group-btn">
                    <button class="btn btn-success" type="button">Buscar</button>
				</span>
			</div>
		</div>
		<div class="col-6">
			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					Compañías
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<?php
					foreach ($json1 as $row1) {
						echo "<a class='dropdown-item' href='#'". $row1['name']. "</a>";
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="row tabla">
		<div class="row justify-content-md-center">
			<?php

			echo "<table class='table table-hover' id='tabla'>";
			echo "<thead>";
			echo "<tr><th scope='row'>Name</th><th scope='row'>Email</th><th scope='row'>Compañía</th><th scope='row'>Phone</th><th scope='row'>Sitio web</th></tr>";
			echo "</thead>";

			foreach ($json as $row) {
				foreach ($json1 as $row1){
					if ( $row['company_id'] == $row1['id'] ) {
						$comp = $row1['name'];
					}
				}
				echo "<tr id='campos'><td id='si'>" . $row['name'] . "</td><td id='si'>" . $row['email'] . "</td><td>" . $comp . "</td><td id='si'>" . $row['phone'] . "</td><td id='si'>" . $row['website'] . "</td></tr>";
			}
			echo "</tbody>";
			echo "</table>";

			?>
		</div>
	</div>
</div>
</body>
<script>
    function buscar() {
        // Declaro mis variables
        var input, filter, table, tr, td, i;
        input = document.getElementById("busc");
        filter = input.value.toUpperCase();
        table = document.getElementById("tabla");
        tr = table.getElementsByTagName("tr");

        // Busco en todas las celdas de la tabla el texto introducido en el input
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            //td = tr[i].getElementsById("si")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" language="javascript" type="text/javascript"></script>
<script src="js/bootstrap.min.js" language="javascript" type="text/javascript"></script>

</html>
